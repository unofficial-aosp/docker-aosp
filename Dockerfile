FROM ubuntu:14.04 AS temp

COPY . .

RUN chmod +x /jdk-6u45-linux-x64.bin && \
    /jdk-6u45-linux-x64.bin && \
    rm /jdk-6u45-linux-x64.bin

FROM ubuntu:14.04

ARG DEBIAN_FRONTEND=noninteractive

COPY --from=temp /jdk1.6.0_45 /usr/lib/jvm/java-6-oracle

ENV J2SDKDIR=/usr/lib/jvm/java-6-oracle \
    J2REDIR=/usr/lib/jvm/java-6-oracle/jre \
    PATH=$PATH:/usr/lib/jvm/java-6-oracle/bin:/usr/lib/jvm/java-6-oracle/db/bin:/usr/lib/jvm/java-6-oracle/jre/bin \
    JAVA_HOME=/usr/lib/jvm/java-6-oracle \
    DERBY_HOME=/usr/lib/jvm/java-6-oracle/db

RUN echo 'dash dash/sh boolean false' | debconf-set-selections && \
    dpkg-reconfigure -p critical dash && \
    apt-get update && \
    apt-get -y install software-properties-common && \
    add-apt-repository ppa:deadsnakes/ppa -y && \
    add-apt-repository ppa:git-core/ppa -y && \
    apt-get update && \
    apt-get -y install git-core gnupg flex bison gperf build-essential zip curl \
                       zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev \
                       x11proto-core-dev libx11-dev lib32z-dev libgl1-mesa-dev libxml2-utils \
                       xsltproc unzip python-networkx python3.6 bc && \
    add-apt-repository --remove ppa:deadsnakes/ppa -y && \
    add-apt-repository --remove ppa:git-core/ppa -y && \
    apt-get -y purge software-properties-common && \
    apt-get -y autoremove --purge && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    curl -L https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 -o /usr/local/bin/dumb-init && \
    chmod +x /usr/local/bin/dumb-init && \
    curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo && \
    chmod +x /usr/local/bin/repo

RUN useradd --create-home aosp && \
    echo -e "[color]\n\tui = auto\n[user]\n\tname = Docker AOSP\n\temail = aosp@aosp.docker.local" > /home/aosp/.gitconfig && \
    chown aosp:aosp /home/aosp/.gitconfig

ENV USE_CCACHE=1 \
    CCACHE_DIR=/tmp/ccache

VOLUME ["/tmp/ccache", "/aosp"]

USER aosp
WORKDIR /aosp

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
CMD ["/bin/bash"]
