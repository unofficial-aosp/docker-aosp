# Docker AOSP

A docker image for a minimal AOSP build environment.

## Includes

- Packages required for building [Android Source](https://source.android.com/setup/build/initializing).
- Oracle Java SE 6 JDK required for building [Android KitKat](https://source.android.com/setup/build/older-versions).
- [Dumb-init](https://github.com/Yelp/dumb-init) to prevent [zombie processes](https://blog.phusion.nl/2015/01/20/docker-and-the-pid-1-zombie-reaping-problem/).

## Usage

```
docker run -it --rm -v <source-directory>:/aosp:rw -v <ccache-directory>:/tmp/ccache:rw registry.gitlab.com/unofficial-aosp/docker-aosp:kitkat
```

## License

This project is licensed under the MIT License - see [LICENSE](LICENSE) for details.

Java SE JDK 6 is licensed under the Oracle BCL License - see [THIRDPARTYLICENSE](THIRDPARTYLICENSE) for details.

Dumb-init is licensed under the MIT License - see [THIRDPARTYLICENSE](THIRDPARTYLICENSE) for details.
